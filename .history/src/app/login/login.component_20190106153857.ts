import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

  constructor(private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        ) { }

  ngOnInit() {
  		this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        this.authenticationService.logout();
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        
        this.authenticationService.login(this.loginForm.value).subscribe(res => {
          console.log(res);
          if(res['status'] == 1){
            this.router.navigate(['/home']);
            localStorage.setItem('currentUser', JSON.stringify(res.user));
          }else{
            alert(res['message']);
            this.loading = false;
          }
        }, error => {
          console.log(error);
            this.loading = false;
        });

    }

}
