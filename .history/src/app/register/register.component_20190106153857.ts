import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

  constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authService : AuthenticationService
        ) { }

  ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
  }

  get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        let userObj = this.registerForm.value;
        userObj['role'] = 'customer';
        this.authService.registerUser(userObj).subscribe(res => {
            if(res.status == 0){
                this.loading = false;
                alert('Username already exists..!');
            }else{            
                alert('User registration successfull.. Please proceed to login..');
                this.router.navigate(['/login']);
            }
        }, error => {
            console.log(error);
            this.loading = false;
        });
    }

}
