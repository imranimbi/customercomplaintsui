import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { AuthenticationService } from '../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser = {};
  complaintForm: FormGroup;
  loading = false;
  submitted = false;
  getPayload;
  selectedComplaint = false;

  constructor(config: NgbModalConfig,
   private modalService: NgbModal,
   private formBuilder: FormBuilder,
   private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.currentUser =  this.authenticationService.getuser();

  	this.complaintForm = this.formBuilder.group({
            heading: ['', Validators.required],
            description: ['', Validators.required]
        });

    this.getComplaints();
  }

  get f() { return this.complaintForm.controls; }

  logout(){
    this.authenticationService.logout();
  }

  getComplaints(){
    if(this.currentUser['role'] == 'customer'){
      this.authenticationService.getComplaints(this.currentUser['userName']).subscribe(res => {
          this.getPayload = res;
      }, error => {
          console.log(error);
      });
    }else{
     this.authenticationService.getAllComplaints().subscribe(res => {
          this.getPayload = res;
      }, error => {
          console.log(error);
      });
    }
  }

  statusChange(complaint){
    this.authenticationService.updateComplaint(complaint).subscribe(res => {
          this.getComplaints();
      }, error => {
          console.log(error);
      });
  }

  addComplaint(){
  	this.submitted = true;
  	if(this.complaintForm.value.heading && this.complaintForm.value.description){
  		let complaintObj = this.complaintForm.value;
  		complaintObj['createdDate'] = new Date();
  		complaintObj['status'] = 'logged';
      complaintObj['createdBy'] = this.currentUser['userName'];

  		//this.getPayload.push(complaintObj);
      this.authenticationService.postComplaints(complaintObj).subscribe(res => {
          this.getComplaints();
      }, error => {
          console.log(error);
      });
  		this.updateModal({});
  		this.closeModal();
  	}
  }

  viewComplaint(complaint, content){
  	this.selectedComplaint = true;
  	this.updateModal(complaint);
    this.openModal(content);
  }

  updateModal(Obj){
  	this.complaintForm.patchValue({
  		 heading: Obj['heading'],
  		 description: Obj['description']
  	});
  }

  openModal(content) {
    this.modalService.open(content, { centered: true });
  }

  closeModal(){
    this.selectedComplaint = false;
    this.modalService.dismissAll();
    this.updateModal({});
  }

}
