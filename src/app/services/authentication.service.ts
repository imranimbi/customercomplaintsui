﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';


@Injectable()
export class AuthenticationService {
    nodeURL = 'http://localhost:3000';
    
    constructor(private http : HttpClient, private router: Router,) { }

    login(userObj):Observable<any> {
        let url = this.nodeURL + '/login';
        return this.http.post(url, userObj);
    }

    registerUser(userObj):Observable<any>{
        let url = this.nodeURL + '/createUser';
        return this.http.post(url, userObj);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }

    getuser(){
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    getComplaints(username):Observable<any>{
        let url = this.nodeURL + '/complaint'+'?id='+username;
        return this.http.get(url);
    }

    getAllComplaints():Observable<any>{
        let url = this.nodeURL + '/listComplaints';
        return this.http.get(url);
    }

    postComplaints(complaintObj):Observable<any>{
        let url = this.nodeURL + '/complaint';
        return this.http.post(url, complaintObj);
    }

    updateComplaint(complaintObj):Observable<any>{
        console.log(complaintObj)
        let url = this.nodeURL + '/complaint';
        return this.http.put(url, complaintObj);
    }
}